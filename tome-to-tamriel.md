<!--
SPDX-FileCopyrightText: © 2023 Carmen Bianca Bakker <carmen@carmenbianca.eu>

SPDX-License-Identifier: CC-BY-4.0
-->

\onecolumn

# Credits {-}

**Written & designed by** Carmen Bianca Bakker

**The Elder Scrolls created by** Bethesda Game Studios

**Savage Worlds written & designed by** Shane Lacy Hensley, with Clint Black

The Elder Scrolls and all associated logos and its respective logos are
trademarks of ZeniMax Media.

This game references the _Savage Worlds_ game system, available from Pinnacle
Entertainment Group at <https://www.peginc.com>. Savage Worlds and all
associated logos and trademarks are copyrights of Pinnacle Entertainment Group.

All parts of this work over which the author holds copyright are licensed under
a
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
© 2023 Carmen Bianca Bakker

![](SWFan.jpg){width=300}

\twocolumn

# Foreword

This is an unpolished work that is for all intents and purposes unpublished. It
may never be properly polished.

## Source and sharing

The source code for this document is available at
<https://gitlab.com/carmenbianca/tome-to-tamriel>. It is written in a single
Markdown file, and should be really easy to fork and change. Pandoc is used to
generate all file formats.

You can find hosted versions of this document at:

- HTML --- <https://carmenbianca.gitlab.io/tome-to-tamriel/>
- EPUB ---
  [https://carmenbianca.gitlab.io/tome-to-tamriel/Wild Cards' Tome to Tamriel.epub](https://carmenbianca.gitlab.io/tome-to-tamriel/Wild%20Cards%27%20Tome%20to%20Tamriel.epub)
- PDF ---
  [https://carmenbianca.gitlab.io/tome-to-tamriel/Wild Cards' Tome to Tamriel.pdf](https://carmenbianca.gitlab.io/tome-to-tamriel/Wild%20Cards%27%20Tome%20to%20Tamriel.pdf)

<!-- As stated in the credits, all parts of this document over which I have rights
are released under an attribution licence. If you wish to create a derivative
version of this document, you are more than free to. In fact, you are encouraged
to. While I love and appreciate feedback, one document can never possibly
account for everybody's tastes and preferences.

When you do create a derivative version, you must at a minimum attribute all
authors of this document, preferably in the relevant section at the top. Also,
please be mindful about not including material for which you have no permissions
from the copyright holders. -->

# Races

<!--
Use Skyrim and ESO for inspiration. Previous games are not accounted for.
-->

## Argonians

- **Amphibious:** Argonians can breathe underwater. <!-- +1 -->
- **Claws:** An Argonian's claws are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage. <!-- +1 -->
- **Disease Resistance:** Argonians receive a +4 bonus to resist disease. Damage
  from disease is reduced by 4. <!-- +0, shared with poison immunity. -->
- **Environmental Weakness (Cold):** Argonians suffer a -4 penalty to resist the
  cold, and suffer +4 damage from cold-based attacks. <!-- -1 -->
- **Immune to Poison:** Argonians are immune to poison <!-- +1 -->

## Bretons

- **Magic Resistance:** Bretons have the Arcane Resistance Edge. <!-- +2 -->

## Dark Elves

- **Dynamic:** Dark Elves start with a d4 in Fighting, Shooting, or
  Spellcasting---their choice. <!-- +1 -->
- **Fire Resistance:** Dark Elves receive a +4 bonus to resist fire-based
  effects (including heat). Damage from these sources is reduced by 4.
  <!-- +1 -->

## High Elves

<!-- meme -->

- **Syrabane's Boon:** High Elves have the Power Points Edge. <!-- +2 -->

## Imperials

- **Adaptable:** Imperials begin play with any Novice Edge of their choosing.
  They must meet its Requirements as usual. <!-- +2 -->

## Khajiit

- **Claws:** A Khajiit's claws are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage. <!-- +1 -->
- **Low Light Vision:** Khajiit ignore penalties for Dim and Dark Illumination.
  <!-- +1 -->

## Nords

- **Cold Resistance:** Nords receive a +4 bonus to resist frost-based effects.
  Damage from these sources is reduced by 4. <!-- +1 -->
- **Stalwart:** Nords have Toughness +1. <!-- +1 -->

## Orcs

- **Brawny:** Orcs have the Soldier Edge. <!-- +2 -->

## Redguards

- **Adrenaline Rush:** Redguards ignore one level of Fatigue penalties.
  <!-- +1 -->
- **Poison Resistance:** Redguards receive a +4 bonus to resist poison. Damage
  from poison is also reduced by 4. <!-- +1 -->

## Wood Elves

- **Archery Expertise:** Wood Elves start with a d4 in Shooting. <!-- +1 -->
- **Poison and Disease Resistance:** Wood Elves receive a +4 bonus to resist
  poison and disease. Damage from poison and disease is also reduced by 4.
  <!-- +1 -->

# Hindrances

TODO

# Skills

TODO

# Edges

TODO

# Gear

TODO

# Setting Rules

## Everyone Speaks Tamrielic

Although you may optionally include other languages, it is generally understood
that everyone speaks some sort of universal language.

# Powers

TODO

# Bestiary

TODO
